###Getting Started###

* Pre-Condition 

1. node version 7.0.0
2. npm is updated to the latest
```
	> sudo npm install npm -g
```
* Checkout this REPO
```
	> git clone https://jkungns@bitbucket.org/nssuitereact/boilerplate.git destinationFolder/
```

* update the PORT # under webpack.config.js
```
	> cd destinationFolder
	> vim webpack.config.js
```

* Ensure webpack and webpack-dev-server is installed and loaded if its not already
```
	> npm install --save-dev webpack
	> npm install --save-dev webpack-dev-server
```
* Run npm install
```
	> npm install
```
* Initiate a new GIT REPO and point the current project/dir to new REPO
```
	> git remote set-url origin https://newrepo.git
```
* Commit and PUSH
```
	> git add .
	> git commit -m ‘initial commit’
	> git push origin master
```
```
